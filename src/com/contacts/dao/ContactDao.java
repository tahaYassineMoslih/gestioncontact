package com.contacts.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.contacts.models.Contact;
import com.gcontact.connexion.DatabaseConnection;

public class ContactDao implements IDao<Contact>{
	private DatabaseConnection conn = DatabaseConnection.getInstance();
    private PreparedStatement stm;
    private Connection _connection;
	@Override
	public Contact insert(Contact obj) {
		try {
			_connection = conn.openConnection();
            stm = _connection.prepareStatement("insert into contact (nom, tel, id_user) values(?,?,?)");
            stm.setString(1,obj.getNom());
            stm.setString(2, obj.getTel());
            stm.setInt(3, obj.getUser().getId());
            if(stm.executeUpdate()>0)
            	return obj;
            stm.close();
//            _connection.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return null;
	}

	@Override
	public void delete(int id) {
		try{
			_connection = conn.openConnection();
            stm = _connection.prepareStatement("delete from contact where id = ?");
            stm.setInt(1, id);
            stm.executeUpdate();
//            _connection.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
	}

	@Override
	public Contact selectById(int id) {
		Contact cl = null;
        try {
        	_connection = conn.openConnection();
            stm = _connection.prepareStatement("select * from contact where id = ?");
            stm.setInt(1, id);
            
            ResultSet rs = stm.executeQuery();
            if(rs.next()) {
                cl = new Contact();
                cl.setId(rs.getInt("id"));
                cl.setNom(rs.getString("nom"));
                cl.setTel(rs.getString("tel"));
//                cl.setUser(user);
            }
            rs.close();
            stm.close();
            _connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cl;
	}

	@Override
	public Contact update(Contact obj) {
		try {
			_connection = conn.openConnection();
            stm = _connection.prepareStatement("update contact set nom = ?, tel = ? where id = ?");
            stm.setString(1,obj.getNom());
            stm.setString(2, obj.getTel());
            stm.setInt(3, obj.getId());
            if(stm.executeUpdate()>0)
            	return obj;
            stm.close();
//            _connection.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return null;
	}

	public List<Contact> getContactsUser(int idUser) {
		List<Contact> contacts = new ArrayList<>();
        
        try {
        	_connection = conn.openConnection();
            stm = _connection.prepareStatement("select * from contact where id_user = ?");
            stm.setInt(1, idUser);
            ResultSet rs = stm.executeQuery();
            while(rs.next())
            	contacts.add(new Contact(rs.getInt("id"), rs.getString("nom"), rs.getString("tel")));
            
            stm.close();
            rs.close();
//            _connection.close();
        } catch (SQLException ex) {
        	ex.printStackTrace();
        }
        
        return contacts;
	}

	@Override
	public List<Contact> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
