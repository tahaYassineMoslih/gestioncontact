package com.contacts.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.contacts.models.User;
import com.gcontact.connexion.DatabaseConnection;

public class UserDao implements IDao<User>{

	private DatabaseConnection conn = DatabaseConnection.getInstance();
	private PreparedStatement stmt;
	private Connection _connection;
	
	public User loginUser(String username,String password)
	{
		User user = null;
		try {			
			_connection = conn.openConnection();
			String req = "SELECT * from user where username like ? AND password like ?";
			stmt = _connection.prepareStatement(req);
			stmt.setString(1, username);
			stmt.setString(2, password);
			
			ResultSet rs = stmt.executeQuery();
			if(rs.next())
			{
				user = new User();
				System.out.println("suername est: " + username);
				user.setId(rs.getInt("id"));
				user.setNom(rs.getString("nom"));
				user.setPrenom(rs.getString("prenom"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
			}
			rs.close();
			stmt.close();
//			_connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User insert(User o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User selectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User update(User o) {
		return o;
	}

	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
