package com.contacts.dao;

import java.util.List;

public interface IDao<T> {
	
	public T insert(T o);
	public void delete(int id);
	public T selectById(int id);
	public T update(T o);
	public List<T> getAll();

}
