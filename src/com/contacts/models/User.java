package com.contacts.models;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import com.contacts.services.UserService;

@ManagedBean
public class User implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String nom;
	private String prenom;
	private String username;
	private String password;
	
	public User() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String login() {
		UserService userS = new UserService();
		User user;
		if( (user = userS.loginUser(username, password)) != null) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userSession", user);
			return "index?faces-redirect=true";
		}
		return "login";
	}

	public String deconnecter() {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("userSession");
		return "login";
	}
}
