package com.contacts.models;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.contacts.services.ContactService;

@ManagedBean(name="dtSelectionView")
@ViewScoped
public class SelectionView implements Serializable {
     
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Contact> contacts;
    private Contact selectedContact;
    private Contact selectedDeleteContact;
    private List<Contact> selectedContacts;
    
    @ManagedProperty("#{contactService}")
    private ContactService service;
     
    @PostConstruct
    public void init() {
    	User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSession");
    	contacts = new ContactService().getContactsUser(user.getId());
    }
 
    public List<Contact> getContacts() {
        return contacts;
    }

     
    public void setService(ContactService service) {
        this.service = service;
    }
 
    public Contact getSelectedContact() {
        return selectedContact;
    }
 
    public void setSelectedContact(Contact selectedContact) {
        this.selectedContact = selectedContact;
    }
 
    public List<Contact> getSelectedContacts() {
        return selectedContacts;
    }
 
    public void setSelectedContacts(List<Contact> selectedContacts) {
        this.selectedContacts = selectedContacts;
    }
     
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Contact Selected", (String.valueOf(((Contact) event.getObject()).getId())));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Contact Unselected", (String.valueOf(((Contact) event.getObject()).getId())));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

	public Contact getSelectedDeleteContact() {
		return selectedDeleteContact;
	}

	public void setSelectedDeleteContact(Contact selectedDeleteContact) {
		this.selectedDeleteContact = selectedDeleteContact;
	}
}