package com.contacts.models;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.contacts.services.ContactService;

@ManagedBean
@SessionScoped
public class Contact implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String nom;
	private String tel;
	private User user;
	
	public Contact() {
	}
	
	public Contact(int id, String nom, String tel) {
		this.id = id;
		this.nom = nom;
		this.tel = tel;
	}
	
	public Contact(String nom, String tel, User user) {
		this.nom = nom;
		this.tel = tel;
		this.user = user;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", nom=" + nom + ", tel=" + tel + ", user=" + user + "]";
	}

	public String updateContact(Contact ct) {
		ContactService ctS = new ContactService();
		if(ctS.updateContact(ct) != null)
			return "index";
		return "index";
	}
	
	public String deleteContact(String id) {
		ContactService ctS = new ContactService();
		ctS.deleteContact(Integer.valueOf(id));
		return "index";
	}
	
	public String addContactView() {
		return "addContact";
	}
	
	public String addContact() {
		ContactService ctS = new ContactService();
		if(ctS.addContact(new Contact(nom, tel, (User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSession"))) != null)
			return "index";
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("error", "Remplir tous les champs");
		return "addContact";
	}
}
