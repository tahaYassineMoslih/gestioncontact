package com.contacts.services;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.contacts.dao.ContactDao;
import com.contacts.models.Contact;

@ManagedBean(name = "contactService")
@ApplicationScoped
public class ContactService {
	
	public List<Contact> getContactsUser(int idUser) {
		return new ContactDao().getContactsUser(idUser);
	}
	
	public Contact updateContact(Contact ct) {
		return new ContactDao().update(ct);
	}
	
	public void deleteContact(int id) {
		new ContactDao().delete(id);
	}
	
	public Contact addContact(Contact obj) {
		return new ContactDao().insert(obj);
	}
}
