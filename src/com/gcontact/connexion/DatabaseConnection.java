package com.gcontact.connexion;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnection {
	
	private static DatabaseConnection instance = null;
	private Connection connection;
	private String url ="jdbc:mysql://localhost:3306/GestionContacts";
	private String username ="root";
	private String password = "";
	
	private DatabaseConnection()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	public Connection openConnection() throws SQLException
	{
		return this.connection;
	}
	
	public void closeConnection() {
		try {
			this.connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static DatabaseConnection getInstance()
	{
		if(instance == null)
			instance = new DatabaseConnection();
		
		return instance;
	}

}
