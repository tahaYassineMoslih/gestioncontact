package com.gcontact.dao;

import java.util.Collection;

public interface IDao<T> {

	public T save(T entity);

	public T update(T entity);

	public Collection<T> selectAll();

	public T fincdById(int id);

	public void delete(int id);
}
